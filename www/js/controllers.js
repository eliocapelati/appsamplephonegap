'use strict';

angular.module('AppPernambucanas.controllers', [])
    .controller('PernambucanasCrtl', function ($scope, $timeout, $mdSidenav, $mdUtil, $log) {
    $scope.toggleLeft = buildToggler('left');
  
    function buildToggler(navID) {
      var debounceFn =  $mdUtil.debounce(function(){
            $mdSidenav(navID)
              .toggle()
              .then(function () {
                $log.debug("seletor " + navID + " foi criado");
              });
          },200);
      return debounceFn;
    }
  }).controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
      $mdSidenav('left').close()
        .then(function () {
          $log.debug("fechou a sidenav");
        });
    };
  });
