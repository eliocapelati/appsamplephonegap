'use strict';

angular.module('AppPernambucanas', [
    'ngTouch',
    'ngRoute',
    'ngAnimate',
    'AppPernambucanas.controllers',
    'ngMaterial'
]).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/listaprodutos', {templateUrl: 'partials/listaprodutos.html', controller: 'PernambucanasCrtl'});
    $routeProvider.otherwise({redirectTo: '/listaprodutos'});
}]);


